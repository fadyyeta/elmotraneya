jQuery(document).ready(function ($) {
	

	"use strict";

	var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);

	jQuery.browser={};(function(){jQuery.browser.msie=false;
	jQuery.browser.version=0;if(navigator.userAgent.match(/MSIE ([0-9]+)\./)){
	jQuery.browser.msie=true;jQuery.browser.version=RegExp.$1;}})();

	// mMenu
	if ($("#mobile_m")[0]) {
		$('#mobile_m').before('<a class="mobile_menu" href="#mobile_m"><i class="T20_bar"> <i class="T20_x"></i> </i></a>');
		$('#mobile_m').mmenu({
			dragOpen: true,
			position: "right",
			zposition: "next"
		}, {
			clone  : true
		});
	}

	// Superfish
	if ($(".sf-menu")[0]) {
		$('.sf-menu').superfish({
			delay:0,
			animation: {
				opacity: 'show', height: 'none'
			},
			speed: 0
		});
	}
	if (isMobile === false) {
		$.stellar({
			horizontalScrolling: false,
			verticalOffset: 0
		});
	}

	if ($("[class^='slider_']")[0]) {
		$("[class^='slider_']").owlCarousel({
			items:1,
			loop: true,
			margin:0,
			nav:true,
			slideSpeed : 500,
			paginationSpeed : 500,
			autoPlay: true,
			autoHeight: true,
			stopOnHover: true,
			singleItem:true,
			navigation : true,
			navText:['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
			rewindNav : true,
			scrollPerPage : false,
			pagination : false
		});
	}
	

	$('.filter_masonry li ul li a').click(function(){
			var filterClass = '.' + $(this).parent().attr('class');
			$('.filter_masonry li.current').removeClass('current');
			$(this).parent().addClass('current');
			$('.folio_grid li').not( filterClass ).not('.invis').toggleClass('invis');
			$('.folio_grid li').filter( filterClass +'.invis' ).toggleClass('invis');
	
			return false;
		});
		

	// Search and Shop popup
	var popupStatus = 0, popupStatus_shop = 0;
	$(".search_icon").click(function() {
		var windowWidth = document.documentElement.clientWidth;
		var windowHeight = document.documentElement.clientHeight;
		var popupHeight = $("#popup").height();
		var popupWidth = $("#popup").width();
		$("#popup").css({
			"top": windowHeight / 2 - popupHeight / 2,
			"left": windowWidth / 2 - popupWidth / 2
		});
		// Aligning bg
		$("#SearchBackgroundPopup").css({"height": windowHeight});
		if (popupStatus === 0) {
			$("#SearchBackgroundPopup").fadeIn(400);
			$("#popup").fadeIn(400);
			popupStatus = 1;
		}
		$('#inputhead').select();
	});
	$(".close_search").click(function() {
		$("#SearchBackgroundPopup").fadeOut();
		$("#popup").fadeOut();
		popupStatus = 0;
	});
	$('#popup, .search_icon').click(function(e) {
		e.stopPropagation();
	});
	


	// Tabs
	var tabs = jQuery('ul.tabs');
	tabs.each(function (i) {
		// get tabs
		var tab = jQuery(this).find('> li > a');
		tab.click(function (e) {
			// get tab's location
			var contentLocation = jQuery(this).attr('href');
			// Let go if not a hashed one
			if (contentLocation.charAt(0) === "#") {
				e.preventDefault();
				// add class active
				tab.removeClass('active');
				jQuery(this).addClass('active');
				// show tab content & add active class
				jQuery(contentLocation).fadeIn(500).addClass('active').siblings().hide().removeClass('active');
			}
		});
	});
	// Accordion
	jQuery("ul.tt-accordion li").each(function () {
		if (jQuery(this).index() > 0) {
			jQuery(this).children(".accordion-content").css('display', 'none');
		} else {
				jQuery(this).addClass('active').find(".accordion-head-sign").append("<i class='fa fa-angle-up'></i>");
				jQuery(this).siblings("li").find(".accordion-head-sign").append("<i class='fa fa-angle-down'></i>");
		}
		jQuery(this).children(".accordion-head").bind("click", function () {
			jQuery(this).parent().addClass(function () {
				if (jQuery(this).hasClass("active")) {
					return;
				} {
					return "active";
				}
			});

			jQuery(this).siblings(".accordion-content").slideDown();
			jQuery(this).parent().find(".accordion-head-sign i").addClass("fa-angle-up").removeClass("fa-angle-down");
			jQuery(this).parent().siblings("li").children(".accordion-content").slideUp();
			jQuery(this).parent().siblings("li").removeClass("active");
			jQuery(this).parent().siblings("li").find(".accordion-head-sign i").removeClass("fa-angle-up").addClass("fa-angle-down");

		});
	});
	// Toggle
	jQuery("ul.tt-toggle li").each(function () {
		jQuery(this).children(".toggle-content:not(.open)").css('display', 'none');
		jQuery(this).find(".toggle-head-sign").html("<i class='fa fa-angle-down'></i>");
		jQuery(this).children(".toggle-head").bind("click", function () {
			if (jQuery(this).parent().hasClass("active")) {
				jQuery(this).parent().removeClass("active");
			} else {
				jQuery(this).parent().addClass("active");
			}
			jQuery(this).find(".toggle-head-sign").html(function () {
				if (jQuery(this).parent().parent().hasClass("active")) {
					return "<i class='fa fa-angle-up'></i>";
				} else {
					return "<i class='fa fa-angle-down'></i>";
				}
			});
			jQuery(this).siblings(".toggle-content").slideToggle();
		});
	});
	jQuery("ul.tt-toggle").find(".toggle-content.active").siblings(".toggle-head").trigger('click');

	// ToTop
	jQuery('#toTop').click(function () {
		jQuery('body,html').animate({
			scrollTop: 0
		}, 1000);
	});
	jQuery("#toTop").addClass("hidett");
	jQuery(window).scroll(function () {
		if (jQuery(this).scrollTop() < 400) {
			jQuery("#toTop").addClass("hidett").removeClass("showtt");
		} else {
			jQuery("#toTop").removeClass("hidett").addClass("showtt");
		}
	});

	// Notification
	$(".notification-close").click(function () {
		$(this).parent().slideUp("slow");
		return false;
	});



	// Tipsy
	$('.toptip').tipsy({fade: true,gravity: 's'});
	$('.bottomtip').tipsy({fade: true,gravity: 'n'});
	$('.righttip').tipsy({fade: true,gravity: 'w'});
	$('.lefttip').tipsy({fade: true,gravity: 'e'});

	// Sticky
	if (isMobile === false) {
		if ($(".my_sticky")[0]){
			$('.my_sticky').before('<div class="Corpse_Sticky"></div>');
			var divHeight = $('.my_sticky').height();
			var stickyNavTop = $('.my_sticky').offset().top;
			var lastScrollTop = 0;
			var stickyNav = function () {
				var scrollTop = $(window).scrollTop();
				if (scrollTop > stickyNavTop) {
					$('.my_sticky').addClass('sticky');
					$('.Corpse_Sticky').css('height', divHeight + 'px');
				} else {
					$('.my_sticky').css('height', 'auto');
					$('.Corpse_Sticky').css('height', 'auto');
					$('.my_sticky').removeClass('sticky');
				}
			};
			$(window).scroll(function(event){
				
				var divHeight = $('.my_sticky').height()+12;
				var smart_sticky = $('.my_sticky.smart_sticky').length;
				var onenav_sticky = $('.sf-menu.OneNav').length;
				
				stickyNav();
				$('.my_sticky').toggleClass('hasScrolled', $(document).scrollTop() >= 100);
	
				var st = $(this).scrollTop();
				var wind_scr = $(window).scrollTop();
				var window_width = $(window).width();
				if(wind_scr > 300){
					if (st > lastScrollTop && smart_sticky > 0 && onenav_sticky === 0 ){
						$('.my_sticky.sticky').addClass('T_off').css({'top' : '-' + divHeight + 'px'});
					} else {
						$('.my_sticky.sticky').removeClass('T_off').css({'top' : '0'});
					}
					lastScrollTop = st;
				}
			});
			$(window).resize(function(){
				var divHeight = $('.my_sticky').height();
				var window_width = $(window).width();
	
				if (window_width <= 959) {
					if($('.my_sticky').hasClass('sticky')){
						$('.my_sticky').removeClass('sticky');
						$('.my_sticky').stop(true).animate({opacity : 0}, 200, function(){
							$('.my_sticky').removeClass('sticky');
							$('.my_sticky').stop(true).animate({opacity : 1}, 200);
							$('.Corpse_Sticky').css('height', 'auto');
						});
					}
				}
			});
		}
		if ($(".minus_margin")[0]) {
			var headHeight = $('.head').height();
			$(window).scroll(function(event){
				var headHeight = $('.head').height();
			});
			$(window).resize(function(){
				var headHeight = $('.head').height();
			});
			$('.minus_margin').css('margin-top', '-' + headHeight + 'px');
		}
		if ($(".minus_margin_full")[0]) {
			var headerHeight = $('header').height();
			$(window).scroll(function(event){
				var headerHeight = $('header').height();
			});
			$(window).resize(function(){
				var headerHeight = $('header').height();
			});
			$('.minus_margin_full').css('margin-top', '-' + headerHeight + 'px');
		}
	}




	
	try {
		$('.equal_height').each(function(){
			var pr = $(this);
			pr.imagesLoaded( function(){

			var mh = 0 ;
			pr.find('.equal_item').each(function(){
				var h = jQuery(this).height();
				if (h>=mh){mh=h;}
			});
			pr.not('.msnry').find('.equal_item').height(mh);
			});	
        });

	} catch(e){}


	// Reload Animation when click on it.
	$('.animated').click(function() {
		
		var $this = $(this);
		$this.addClass('resetanimation');
		
		var delay=500;
		setTimeout(function(){
		  $this.removeClass('resetanimation');
		}, delay); 
	});
	
	

	//Loading
	if ($(".loading")[0]) {
		var wh = $(window).height();
		$('.sk-wrap').css('marginTop',(wh/2)-20+'px').show();
		
		//Page Transition
		$('a:not([target="_blank"]):not([href^="#"]):not([href*="?replytocom"]):not([href*="?add-to-cart"])').click(function(event){
			event.preventDefault();
			var linkLocation = this.href;
			$(".loading").fadeIn($(".loading").attr('data-speed'), redirectPage(linkLocation));      
		}); 
		
		window.onpageshow = function(event) {
			if (event.persisted) {
				window.location.reload(true) ;
			}
		};
	}

	
	// Call FlexSlider
	bizpro_flexslider();
	
	// Call Owl Carousel
	bizpro_owlcarousel();
	
	// Call Kwicks Slider
	bizpro_kwicks();
	
	// Call Testimonials
	bizpro_testimonials();
	
	// Call News Ticker
	bizpro_newsticker();
	
	// Call Countdown Timer 
	bizpro_countdown();
	
	// Call Progressbar
	bizpro_progressbar();
	
	
	// Nivo Slider
	if ($("#nivo-slider")[0]) {
		$("#nivo-slider").each(function() {
			var $this = $(this);
			$this.nivoSlider({
			   animSpeed: $this.attr('data-speed'),
			   pauseTime: $this.attr('data-pause'),
			   nextText:'<i class="fa fa-angle-right "></i>',
			   prevText:'<i class="fa fa-angle-left "></i>'
			});
		});
	}
	
	
	// RoundAbout Slider
	if ($(".roundabout ul")[0]) {
		$(".roundabout ul").each(function() {
			var $this = $(this);
			$this.roundabout({
				autoplay:true,
				autoplayInitialDelay:1000,
				autoplayDuration:$this.attr('data-pause'),
				autoplayPauseOnHover:true,
				minOpacity:1,
				duration: $this.attr('data-speed'),
				easing: 'easeOutQuad',
				enableDrag: true,
				responsive:true		
			});
		});
	}
	
	
	// LiteAccordion Slider
	if ($("#ACCslider")[0]) {
		$("#ACCslider").each(function() {
			var $this = $(this);
			var display_width = parseInt($(window).width());
			var item_width = $this.attr('data-width');
			var item_height = $this.attr('data-height');
			var item_ratio=1;
			if (display_width < parseInt(item_width)+58 ){
				if (display_width <= 658 ){	
					display_width=658;
					}
				item_height=((display_width-58)*item_height)/item_width;
				item_width=display_width-58;
				
				item_ratio=(display_width/(item_width+58));
				
				}
			
			$this.liteAccordion({
					onTriggerSlide : function() {
						this.find('figcaption').fadeOut();
					},
					onSlideAnimComplete : function() {
						this.find('figcaption').fadeIn();
					},
					containerWidth:item_width,
					containerHeight:item_height,
					headerWidth:48,
					cycleSpeed : $this.attr('data-pause'),
					slideSpeed : $this.attr('data-speed'),
					activateOn : 'click', // Or mouseover
					autoPlay : false,
					pauseOnHover : true,
					rounded : false,
					enumerateSlides : true,
					theme : $this.attr('data-theme'),
					easing: 'easeOutExpo'
				}).find('figcaption:first').show();
				$(this).css('transform','scale('+item_ratio+')');
				var ua = window.navigator.userAgent;
        		var msie = ua.indexOf("MSIE ");
				if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)){
					$('#ACCslider').addClass('liteAccordion-ie');	
				}
				
				
				$(window).resize(function(){
				var window_width = $(window).width();
				var item_width = $("#ACCslider").width();
				if (window_width <= 1180) {
					var item_ratio=(window_width/(item_width+58));
					$("#ACCslider").css('transform','scale('+item_ratio+')');
						
					}
				});
			
		});
	}
	

	//3D Slider
	if (jQuery(".sb-slider")[0]) {
		jQuery(function($) {
		var Page = (function() {
			var $navArrows = $( '#nav-arrows' ),
				$shadow = $( '#shadow' ).hide(),
				slicebox = $( '#sb-slider' ).slicebox( {
					onReady : function() {
						$navArrows.show();
						$shadow.show();
					},
					// (v)ertical, (h)orizontal or (r)andom
					orientation : jQuery(".sb-slider").attr('data-orientation'),
					// number of slices / cuboids
					// needs to be an odd number 15 => number > 0 (if you want the limit higher, change the _validate function).
					cuboidsCount : 5,
					// if true then the number of slices / cuboids is going to be random (cuboidsCount is overwitten)
					cuboidsRandom : true,
					// animation speed
					// this is the speed that takes "1" cuboid to rotate
					speed : jQuery(".sb-slider").attr('data-speed'),
					// transition easing
					easing : 'ease',
					// if true the slicebox will start the animation automatically
					autoplay : true,
					// time (ms) between each rotation, if autoplay is true
					interval: jQuery(".sb-slider").attr('data-pause'),
				} ),
				
				init = function() {
					initEvents();
				},
				initEvents = function() {
					// add navigation events
					$navArrows.children( ':first' ).on( 'click', function() {
						slicebox.next();
						return false;
					} );
					$navArrows.children( ':last' ).on( 'click', function() {
						slicebox.previous();
						return false;
					} );
				};
				return { init : init };
		})();
		Page.init();
		});
	}	
	
	
	
	// Masonry
	if ($("#masonry-container")[0]) {
			var msnry = $('#masonry-container');
			var msnrychk = $('.post:not(.format-audio)');
            msnrychk.imagesLoaded( function(){
               msnry.masonry({
                    itemSelector: '.post',
                    isAnimated: true,
                    columnWidth: 1
                });
            });
	}
	
	
	// One Page
	if ($(".OneNav")[0]){
			$('body').plusAnchor({
				easing: 'easeInOutExpo',
				speed:  1000
			});
			
			$('.OneNav li a').click(function(){
				$('.OneNav li.current-menu-item').removeClass('current-menu-item');
				$(this).parent('li').addClass('current-menu-item');
			});
			
			var lastId;
			var topMenu = $(".OneNav");
			var menuItems = topMenu.find("a");
			
			var scrollItems = menuItems.map(function(){
				var u = $(this).attr("href");
				var n = u.indexOf("#"); 
				if(n>=0){
					var item = $(u);
					if (item.length) { return item; }
					}
				
			});

			$(window).scroll(function(){
				
				 // Get container scroll position
				var fromTop = $(this).scrollTop()+100;
				
				// Get id of current scroll item
				var cur = scrollItems.map(function(){
										if ($(this).offset().top < fromTop)
										return this;
										});
				// Get the id of the current element
				cur = cur[cur.length-1];
				
				var id = cur && cur.length ? cur[0].id : "";
		
				if (lastId !== id) {
					lastId = id;
					menuItems.parent().removeClass("current-menu-item").
					end().filter("[href=\\#"+id+"]").parent().addClass("current-menu-item");
				}
				
			         
			});
			
		} 


	// Portfolio Masonry
	if ($(".msnry")[0]) {
            var msnryp = jQuery('.msnry');
            msnryp.imagesLoaded( function(){
               _msnry(msnryp);
            });
	}
	
	
	//Share Buttons
	if ($(".btn-share.btn-twitter")[0]) {
		! function (d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0],
				p = /^http:/.test(d.location) ? 'http' : 'https';
			if (!d.getElementById(id)) {
				js = d.createElement(s);
				js.id = id;
				js.src = p + '://platform.twitter.com/widgets.js';
				fjs.parentNode.insertBefore(js, fjs);
			}
		}(document, 'script', 'twitter-wjs');
	}
	
	
	if ($(".btn-share.btn-facebook")[0]) {
		(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s); js.id = id;
			js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=138798126277575";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	}
	
	if ($(".btn-share.btn-linkedin")[0]) {
		$(".btn-share.btn-linkedin").html('<script type="IN/Share" data-counter="top"></script>');
		$.getScript("http://platform.linkedin.com/in.js");
	}
	
	if ($(".btn-share.btn-xing")[0]) {
		$(".btn-share.btn-xing").html('<script data-counter="top" type="XING/Share"></script>');
		;(function (d, s) {
			var x = d.createElement(s),
			s = d.getElementsByTagName(s)[0];
			x.src = "https://www.xing-share.com/js/external/share.js";
			s.parentNode.insertBefore(x, s);
		})(document, "script");
	}
	
	if ($(".btn-share.btn-google")[0]) {
		$.getScript("http://apis.google.com/js/plusone.js");
	}
	

	
	
// End Ready
});


function redirectPage(linkLocation) {
	"use strict";
	window.location = linkLocation;
}



jQuery( window ).load(function($) {
	"use strict";

	// Loading
	if (jQuery(".loading")[0]) {
		jQuery(".loading").fadeOut(jQuery(".loading").attr('data-speed'));
	}
	
	//Sticky Widget
	var __sh = jQuery('.sidebar').height();
	var __ph = jQuery('.posts').height();
	if (__sh > __ph){jQuery('.posts').height(__sh);}else{jQuery('.sidebar').height(__ph);}
	jQuery(".sticky_widget").stick_in_parent({offset_top:25})
		.on("sticky_kit:bottom", function(e) {
			var elm = jQuery( e.target );
			elm.addClass("stuck_bottom");
  		}).on("sticky_kit:unbottom", function(e) {
			var elm = jQuery( e.target );
			elm.removeClass("stuck_bottom");
  		}).on("sticky_kit:unstick", function(e) {
			var elm = jQuery( e.target );
			elm.css("marginTop","0px");
  		});
});


// Portfolio Masonry
function _msnry(m){
	"use strict";
	m.masonry({
		itemSelector: '.msnry li',
		isAnimated: true,
		animate: true,
		animationOptions: {
			duration: 1000,
			queue: false
		},
		columnWidth: 1
	});

}

// FlexSlider Function
function bizpro_flexslider(){
	"use strict";
	if (jQuery(".flexslider")[0]) {
		jQuery(".flexslider").each(function() {
			var $this = jQuery(this);
			$this.flexslider({
				animation: $this.attr('data-animation'),
				direction: $this.attr('data-direction'),
				slideshowSpeed: $this.attr('data-pause'),
				animationSpeed: $this.attr('data-speed'),
				prevText: "",
				nextText: "",
				controlNav: false,
				keyboardNav: true,
				slideshow:$this.attr('data-autoplay'),
				start: function(slider) {
					jQuery(slider).find('h3').delay(100).addClass('effect').fadeIn(400);
					jQuery(slider).find('p').delay(100).addClass('effectt').fadeIn(400);
				},
				after: function(slider) {
					jQuery(slider).find('h3').delay(100).addClass('effect').fadeIn(400).removeClass('out');
					jQuery(slider).find('p').delay(100).addClass('effectt').fadeIn(400).removeClass('out');
				}
			});
		});
	}

}



// Owl Carousel Function
function bizpro_owlcarousel(){
	"use strict";
	if (jQuery(".owl-carousel")[0]) {
		jQuery(".owl-carousel").each(function() {
			var $this = jQuery(this);
			$this.owlCarousel({
				items:$this.attr('data-col'),
				loop: true,
				margin:15,
				nav:true,
				slideSpeed:$this.attr('data-speed'),
				navText:['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
				navigation: false,
				pagination: false,
				paginationNumbers: false,
				autoPlay:true,
				rtl : false,
				autoplayTimeout:$this.attr('data-pause'),
				paginationSpeed:$this.attr('data-speed'),
				autoplayHoverPause:true,
				responsive : {0 : {items : 1,nav : false}, 480 : {items : 2}, 768 : {items : $this.attr('data-col')}}
			});
		});
	}
	
}


	
// Kwicks Slider Function
function bizpro_kwicks(){
	"use strict";

	// Kwicks Slider
	if (jQuery(".kwicks")[0]) {
		jQuery(".kwicks").each(function() {
			var $this = jQuery(this);
			var _id = $this.attr('id');
			var _behavior = 'menu';
			if ($this.data('autoplay')){_behavior = 'slideshow';}
			
			jQuery('head').append('<style id="custom_'+_id+'" type="text/css"></style>');
			
			$this.kwicks({
				maxSize : '55%',
				spacing : $this.data('margin'),
				behavior: _behavior,
				interval:$this.data('duration')
			});
			
			$this.find('li').each(function() {
				var $this = jQuery(this);
				var __id = $this.attr('id');
				jQuery('style#custom_'+_id).append('#'+__id+'{background:url("'+$this.attr('data-background')+'") no-repeat center center;}');
			});
			
			// Set Caption size
			var _kws = $this;
			var _kwm =$this.attr('data-margin');
			_kws.imagesLoaded( function(){
				_kwm = parseInt(_kwm);
				var _kww = jQuery(_kws).width();
				var _kwi = jQuery(_kws).find('a').length;
				var _kwc = parseInt((_kww -((_kwi-1)*_kwm))*0.55)-50;
				jQuery(_kws).find('.kwick_caption_text').width(_kwc);
			});
		});
	}
	
}


// Testimonials Function
function bizpro_testimonials(){
	"use strict";
	
	if (jQuery(".bizpro_testimonials")[0]) {
		jQuery(".bizpro_testimonials").each(function() {
			var $this = jQuery(this);
			var _id1 = $this.find('.custom_block2').attr('id');
			var _id2 = $this.find('.custom_block2_testimonial').attr('id');

			jQuery('#'+_id1+' ul li:first-child').addClass('active');
			jQuery('#'+_id2+'.custom_block2_testimonial .h_slider:first-child').addClass('showme');
			jQuery('#'+_id1+' ul li a').mouseover(function (e) {
				e.preventDefault();
				var slide_id = jQuery(this).attr('href');
				var slide_post_id = slide_id.replace('#', '');
				jQuery(this).closest('ul').find('li').removeClass('active');
				jQuery(this).parent().addClass('active');
				jQuery('#'+_id2+'.custom_block2_testimonial .h_slider').removeClass('showme');
				jQuery('#'+_id2+'.custom_block2_testimonial .h_slider'+' .'+slide_post_id).parent().addClass('showme').stop(true, true);
			});
			var toggleTestimonial = function(e){
				jQuery("#"+_id1+" ul li.active").removeClass().next().add("#"+_id1+" ul li:first").last().addClass("active");
				jQuery("#"+_id2+".custom_block2_testimonial .h_slider.showme").removeClass('showme').next().add("#"+_id2+".custom_block2_testimonial .h_slider:first").last().addClass("showme");
			};
			var timer;
			function startTimer() {
				timer = setInterval(toggleTestimonial, 3500);
			}
			jQuery('#'+_id1+' ul li, #'+_id2+'.custom_block2_testimonial .h_slider').hover(function (ev) {
				clearInterval(timer);
			}, function (ev) {
				startTimer();
			});
			startTimer();
	
		});
	}

}




// News Ticker Function
function bizpro_newsticker(){
	"use strict";
	if (jQuery(".news_ticker")[0]) {
		jQuery(".news_ticker").each(function() {
			jQuery(this).liScroll({travelocity: 0.08});
		});
	}
	
}
	
// Countdown Timer Function
function bizpro_countdown(){
	"use strict";	
	if (jQuery(".countdown")[0]) {
		jQuery(".countdown").each(function() {
			var $this = jQuery(this);
			$this.countdown({
				date: $this.attr('data-date'),
				format: "on"
				});
		});
	}
}
	
// Progressbar
function bizpro_progressbar(){
	"use strict";	
	if (jQuery(".progress-bar.style-static")[0]) {
		jQuery( ".progress-bar.style-static > span" ).each(function() {
			var $this = jQuery(this);
			jQuery(this).addClass('Starting');
			$this.css("width", $this.attr('data-rel') + "%");
		});
	}
}	


// Shop Popup
function shop_popup(){
	"use strict";
	var popupStatus_shop = 0;
	var windowWidth_shop = document.documentElement.clientWidth;
	var windowHeight_shop = document.documentElement.clientHeight;
	var popupHeight_shop = jQuery("#popup_shop").height();
	var popupWidth_shop = jQuery("#popup_shop").width();
	jQuery("#popup_shop").css({
		"top": windowHeight_shop / 2 - popupHeight_shop / 2,
		"left": windowWidth_shop / 2 - popupWidth_shop / 2
	});
	jQuery("#SearchBackgroundPopup_shop").css({"height": windowHeight_shop});
	if (popupStatus_shop === 0) {
		jQuery("#SearchBackgroundPopup_shop").fadeIn(400);
		jQuery("#popup_shop").fadeIn(400);
		popupStatus_shop = 1;
	}
}

function close_shop_popup(){
		"use strict";
		jQuery("#SearchBackgroundPopup_shop").fadeOut();
		jQuery("#popup_shop").fadeOut();
	}



// Process Bar
function isElementVisible($elementToBeChecked){
	"use strict";
	var TopView = jQuery(window).scrollTop();
	var BotView = TopView + jQuery(window).height();
	var TopElement = $elementToBeChecked.offset().top;
	var BotElement = TopElement + $elementToBeChecked.height();
	return ((BotElement <= BotView) && (TopElement >= TopView));
}

jQuery(window).scroll(function () {
	
	"use strict";
	//Check Sticky Widgets
	var hdr = jQuery('.my_sticky.hasScrolled.sticky:not(.T_off)');
	var hdrstk = hdr.length;
	
	if (hdrstk>0){
			var ht = hdr.height();
			jQuery('.widget.is_stuck:not(.stuck_bottom)').css("marginTop",ht+"px");
		}else{
			jQuery('.widget.is_stuck:not(.stuck_bottom)').css("top","25px");
			jQuery('.widget.is_stuck:not(.stuck_bottom)').css("marginTop","0px");
			}

	
	//Progress bar
	jQuery( ".progress-bar.style-animation > span" ).each(function() {
		var $this = jQuery(this);
		var isOnView = isElementVisible(jQuery(this));
		if(isOnView && !jQuery(this).hasClass('Starting')){
			jQuery(this).addClass('Starting');
			$this.animate({
				width: $this.attr('data-rel') + "%"
			}, 400);
		}
	});
	
	

	// Animations
	if(jQuery(window).width() > 768) {
		jQuery('.animated').css('visibility', 'hidden');
		jQuery(".animated:not(.starting)").each(function() {
			var $this = jQuery(this);
			var isOnView = isElementVisible(jQuery(this));
			if(isOnView && !jQuery(this).hasClass('Starting')){
				var anim = $this.attr("data-add");
				var delay = ($this.attr("data-delay") ? $this.attr("data-delay") : 1);
				if(delay > 1){$this.css("animation-delay", delay + "ms");}
				if(delay > 1){$this.css("-webkit-animation-delay", delay + "ms");}
				jQuery(this).addClass('visible').addClass('Starting').addClass(anim);
				}
			
		});
		
	}
	

});


// Portfolio Filter
(function(e) {
	"use strict";

		e.fn.sorted = function(t) {
	        var n = {
	            reversed: !1,
	            by: function(e) {
	                return e.text();
	            }
	        };
			var $data = e(this);
			var arr = $data.get();
	        return e.extend(n, t), $data , arr , arr.sort(function(t, r) {
	            var i = n.by(e(t)),
	                s = n.by(e(r));
	            return n.reversed ? i < s ? 1 : i > s ? -1 : 0 : i < s ? -1 : i > s ? 1 : 0
	        }), e(arr)
	    }

	})(jQuery), 
	jQuery(function($) {
	    var e = function(e) {
	            var t = {
	                current: !1,
	                type: 0
	            };
	            for (var n = 0; n < e.length; n++) e[n].indexOf("current") == 0 && (t.current = !0), e[n].indexOf("filter") == 0 && (t.filter = e[n].split("-")[1]);
	            return t
	        },
	        t = function(e) {
	            var t = e.parent().filter('[class*="current"]');
	            return t.find("a").attr("data-value")
	        },
	        n = function(e) {
	            var t = e.parent().filter('[class*="current"]');
	            return t.find("a").attr("data-value")
	        },
	        r = {
	            duration: 800,
	            easing: "easeInOutQuad",
	            adjustHeight: "dynamic",
		enhancement : function(c) { bizpro_lightbox(); },
	            useScaling: !0
	        },
	        i = $("#list"),
	        s = i.clone(),
	        o = $("ul.splitter ul");
	    o.each(function(u) {
	        var a = $(this),
	            f = a.find("a");
	        f.bind("click", function(u) {
	            var a = $(this),
	                l = a.parent(),
	                c = e(l.attr("class").split(" ")),
	                h = c.current,
	                p = c.filter;
	            if (!h) {
	                f.parent().removeClass("current").removeClass("current").removeClass("current"), l.addClass("current");
	                var d = t(o.eq(1).find("a")),
	                    v = n(o.eq(0).find("a"));
	                if (v == "all") var m = s.find("li");
	                else var m = s.find("li." + v);
	                if (d == "like") { var g = m.sortedd({
	                    by: function(e) {
	                        return parseFloat($(e).find(".likes").attr("original-title"))
	                    }
	                });
	               } else if (d == "date"){ var g = m.sorted({
	                    by: function(e) {
	                        return parseFloat($(e).attr("data-id"))
	                    }
	                });
	               } else { var g = m.sorted({
	                    by: function(e) {
	                        return $(e).find("h3").text().toLowerCase()
	                    }
	                }); }
	                i.quicksand(g, r)
	            }
	            u.preventDefault()
	        })
	    });
		
		bizpro_lightbox();
		
	});


