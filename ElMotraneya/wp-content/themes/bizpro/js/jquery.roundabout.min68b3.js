/*! 
 * jquery.event.drop - v 2.0.0 
 * Copyright (c) 2010 Three Dub Media - http://threedubmedia.com
 * Open Source MIT License - http://threedubmedia.com/code/license
 */
// Created: 2008-06-04 
// Updated: 2010-06-08
// REQUIRES: jquery 1.4.2+, jquery.event.drag 2+

;(function($){ // secure $ jQuery alias

// Events: drop, dropinit, dropstart, dropend

// add the jquery instance method
$.fn.drop = function( str, arg, opts ){
	// figure out the event type
	var type = typeof str == "string" ? str : "",
	// figure out the event handler...
	fn = $.isFunction( str ) ? str : $.isFunction( arg ) ? arg : null;
	// fix the event type
	if ( type.indexOf("drop") !== 0 ) 
		type = "drop"+ type;
	// were options passed
	opts = ( str == fn ? arg : opts ) || {};
	// trigger or bind event handler
	return fn ? this.bind( type, opts, fn ) : this.trigger( type );
};

// DROP MANAGEMENT UTILITY
// returns filtered drop target elements, caches their positions
$.drop = function( opts ){ 
	opts = opts || {};
	// safely set new options...
	drop.multi = opts.multi === true ? Infinity : 
		opts.multi === false ? 1 : !isNaN( opts.multi ) ? opts.multi : drop.multi;
	drop.delay = opts.delay || drop.delay;
	drop.tolerance = $.isFunction( opts.tolerance ) ? opts.tolerance : 
		opts.tolerance === null ? null : drop.tolerance;
	drop.mode = opts.mode || drop.mode || 'intersect';
};

// local refs (increase compression)
var $event = $.event, 
$special = $event.special,
// configure the drop special event
drop = $.event.special.drop = {

	// these are the default settings
	multi: 1, // allow multiple drop winners per dragged element
	delay: 20, // async timeout delay
	mode: 'overlap', // drop tolerance mode
		
	// internal cache
	targets: [], 
	
	// the key name for stored drop data
	datakey: "dropdata",
	
	// the namespace for internal live events
	livekey: "livedrop",
	
	// count bound related events
	add: function( obj ){ 
		// read the interaction data
		var data = $.data( this, drop.datakey );
		// count another realted event
		data.related += 1;
		// bind the live "dropinit" delegator
		if ( !data.live && obj.selector ){
			data.live = true;
			$event.add( this, "dropinit."+ drop.livekey, drop.delegate );
		}
	},
	
	// forget unbound related events
	remove: function(){
		$.data( this, drop.datakey ).related -= 1;
	},
	
	// configure the interactions
	setup: function(){
		// check for related events
		if ( $.data( this, drop.datakey ) ) 
			return;
		// initialize the drop element data
		var data = { 
			related: 0,
			active: [],
			anyactive: 0,
			winner: 0,
			location: {}
		};
		// store the drop data on the element
		$.data( this, drop.datakey, data );
		// store the drop target in internal cache
		drop.targets.push( this );
	},
	
	// destroy the configure interaction	
	teardown: function(){ 
		// check for related events
		if ( $.data( this, drop.datakey ).related ) 
			return;
		// remove the stored data
		$.removeData( this, drop.datakey );
		// remove the "live" delegation
		$event.remove( this, "dropinit", drop.delegate );
		// reference the targeted element
		var element = this;
		// remove from the internal cache
		drop.targets = $.grep( drop.targets, function( target ){ 
			return ( target !== element ); 
		});
	},
	
	// shared event handler
	handler: function( event, dd ){ 
		// local vars
		var results, $targets;
		// make sure the right data is available
		if ( !dd ) 
			return;
		// handle various events
		switch ( event.type ){
			// draginit, from $.event.special.drag
			case 'mousedown': // DROPINIT >>
				// collect and assign the drop targets
				$targets =  $( drop.targets );
				if ( typeof dd.drop == "string" )
					$targets = $targets.filter( dd.drop );
				// reset drop data winner properties
				$targets.each(function(){
					var data = $.data( this, drop.datakey );
					data.active = [];
					data.anyactive = 0;
					data.winner = 0;
				});
				// set available target elements
				dd.droppable = $targets;
				// hold any live elements 
				drop.delegates = [];
				// activate drop targets for the initial element being dragged
				$special.drag.hijack( event, "dropinit", dd ); 
				// flatten all the live elements
				drop.delegates = $.unique( $special.drag.flatten( drop.delegates ) );
				break;
			// drag, from $.event.special.drag
			case 'mousemove': // TOLERATE >>
				drop.event = event; // store the mousemove event
				if ( !drop.timer )
					// monitor drop targets
					drop.tolerate( dd ); 
				break;
			// dragend, from $.event.special.drag
			case 'mouseup': // DROP >> DROPEND >>
				drop.timer = clearTimeout( drop.timer ); // delete timer	
				if ( dd.propagates ){
					$special.drag.hijack( event, "drop", dd ); 
					$special.drag.hijack( event, "dropend", dd ); 
					// cleanout live events...
					$.each( drop.delegates || [], function(){
						$event.remove( this, '.'+ drop.livekey );							
					});
				}
				break;
		}
	},
	
	// identify potential delegate elements
	delegate: function( event ){
		// local refs
		var elems = [], $targets, 
		// element event structure
		events = $.data( this, "events" ) || {};
		// query live events
		$.each( events.live || [], function( i, obj ){
			// no event type matches
			if ( obj.preType.indexOf("drop") !== 0 )
				return;
			// locate the elements to delegate
			$targets = $( event.currentTarget ).find( obj.selector );
			// no element found
			if ( !$targets.length ) 
				return;
			// take each target...
			$targets.each(function(){
				// add an event handler
				$event.add( this, obj.origType +'.'+ drop.livekey, obj.origHandler, obj.data );
				// remember new elements
				if ( $.inArray( this, elems ) < 0 )
					elems.push( this );	
			});	
		});
		drop.delegates.push( elems );
		return elems.length ? $( elems ) : false;
	},
	
	// returns the location positions of an element
	locate: function( elem, index ){ 
		var data = $.data( elem, drop.datakey ),
		$elem = $( elem ), 
		posi = $elem.offset() || {}, 
		height = $elem.outerHeight(), 
		width = $elem.outerWidth(),
		location = { 
			elem: elem, 
			width: width, 
			height: height,
			top: posi.top, 
			left: posi.left, 
			right: posi.left + width, 
			bottom: posi.top + height
		};
		// drag elements might not have dropdata
		if ( data ){
			data.location = location;
			data.index = index;
			data.elem = elem;
		}
		return location;
	},
	
	// test the location positions of an element against another OR an X,Y coord
	contains: function( target, test ){ // target { location } contains test [x,y] or { location }
		return ( ( test[0] || test.left ) >= target.left && ( test[0] || test.right ) <= target.right
			&& ( test[1] || test.top ) >= target.top && ( test[1] || test.bottom ) <= target.bottom ); 
	},
	
	// stored tolerance modes
	modes: { // fn scope: "$.event.special.drop" object 
		// target with mouse wins, else target with most overlap wins
		'intersect': function( event, proxy, target ){
			return this.contains( target, [ event.pageX, event.pageY ] ) ? // check cursor
				1e9 : this.modes.overlap.apply( this, arguments ); // check overlap
		},
		// target with most overlap wins	
		'overlap': function( event, proxy, target ){
			// calculate the area of overlap...
			return Math.max( 0, Math.min( target.bottom, proxy.bottom ) - Math.max( target.top, proxy.top ) )
				* Math.max( 0, Math.min( target.right, proxy.right ) - Math.max( target.left, proxy.left ) );
		},
		// proxy is completely contained within target bounds	
		'fit': function( event, proxy, target ){
			return this.contains( target, proxy ) ? 1 : 0;
		},
		// center of the proxy is contained within target bounds	
		'middle': function( event, proxy, target ){
			return this.contains( target, [ proxy.left + proxy.width * .5, proxy.top + proxy.height * .5 ] ) ? 1 : 0;
		}
	},	
	
	// sort drop target cache by by winner (dsc), then index (asc)
	sort: function( a, b ){
		return ( b.winner - a.winner ) || ( a.index - b.index );
	},
		
	// async, recursive tolerance execution
	tolerate: function( dd ){		
		// declare local refs
		var i, drp, drg, data, arr, len, elem,
		// interaction iteration variables
		x = 0, ia, end = dd.interactions.length,
		// determine the mouse coords
		xy = [ drop.event.pageX, drop.event.pageY ],
		// custom or stored tolerance fn
		tolerance = drop.tolerance || drop.modes[ drop.mode ];
		// go through each passed interaction...
		do if ( ia = dd.interactions[x] ){
			// check valid interaction
			if ( !ia )
				return; 
			// initialize or clear the drop data
			ia.drop = [];
			// holds the drop elements
			arr = []; 
			len = ia.droppable.length;
			// determine the proxy location, if needed
			if ( tolerance )
				drg = drop.locate( ia.proxy ); 
			// reset the loop
			i = 0;
			// loop each stored drop target
			do if ( elem = ia.droppable[i] ){ 
				data = $.data( elem, drop.datakey );
				drp = data.location;
				if ( !drp ) continue;
				// find a winner: tolerance function is defined, call it
				data.winner = tolerance ? tolerance.call( drop, drop.event, drg, drp ) 
					// mouse position is always the fallback
					: drop.contains( drp, xy ) ? 1 : 0; 
				arr.push( data );	
			} while ( ++i < len ); // loop 
			// sort the drop targets
			arr.sort( drop.sort );			
			// reset the loop
			i = 0;
			// loop through all of the targets again
			do if ( data = arr[ i ] ){
				// winners...
				if ( data.winner && ia.drop.length < drop.multi ){
					// new winner... dropstart
					if ( !data.active[x] && !data.anyactive ){
						// check to make sure that this is not prevented
						if ( $special.drag.hijack( drop.event, "dropstart", dd, x, data.elem )[0] !== false ){ 	
							data.active[x] = 1;
							data.anyactive += 1;
						}
						// if false, it is not a winner
						else
							data.winner = 0;
					}
					// if it is still a winner
					if ( data.winner )
						ia.drop.push( data.elem );
				}
				// losers... 
				else if ( data.active[x] && data.anyactive == 1 ){
					// former winner... dropend
					$special.drag.hijack( drop.event, "dropend", dd, x, data.elem ); 
					data.active[x] = 0;
					data.anyactive -= 1;
				}
			} while ( ++i < len ); // loop 		
		} while ( ++x < end ) // loop
		// check if the mouse is still moving or is idle
		if ( drop.last && xy[0] == drop.last.pageX && xy[1] == drop.last.pageY ) 
			delete drop.timer; // idle, don't recurse
		else  // recurse
			drop.timer = setTimeout(function(){ 
				drop.tolerate( dd ); 
			}, drop.delay );
		// remember event, to compare idleness
		drop.last = drop.event; 
	}
	
};

// share the same special event configuration with related events...
$special.dropinit = $special.dropstart = $special.dropend = drop;

})(jQuery); // confine scope	

/*! 
 * jquery.event.drag - v 2.2
 * Copyright (c) 2010 Three Dub Media - http://threedubmedia.com
 * Open Source MIT License - http://threedubmedia.com/code/license
 */
// Created: 2008-06-04 
// Updated: 2012-05-21
// REQUIRES: jquery 1.7.x

;(function( $ ){

// add the jquery instance method
$.fn.drag = function( str, arg, opts ){
	// figure out the event type
	var type = typeof str == "string" ? str : "",
	// figure out the event handler...
	fn = $.isFunction( str ) ? str : $.isFunction( arg ) ? arg : null;
	// fix the event type
	if ( type.indexOf("drag") !== 0 ) 
		type = "drag"+ type;
	// were options passed
	opts = ( str == fn ? arg : opts ) || {};
	// trigger or bind event handler
	return fn ? this.bind( type, opts, fn ) : this.trigger( type );
};

// local refs (increase compression)
var $event = $.event, 
$special = $event.special,
// configure the drag special event 
drag = $special.drag = {
	
	// these are the default settings
	defaults: {
		which: 1, // mouse button pressed to start drag sequence
		distance: 0, // distance dragged before dragstart
		not: ':input', // selector to suppress dragging on target elements
		handle: null, // selector to match handle target elements
		relative: false, // true to use "position", false to use "offset"
		drop: true, // false to suppress drop events, true or selector to allow
		click: false // false to suppress click events after dragend (no proxy)
	},
	
	// the key name for stored drag data
	datakey: "dragdata",
	
	// prevent bubbling for better performance
	noBubble: true,
	
	// count bound related events
	add: function( obj ){ 
		// read the interaction data
		var data = $.data( this, drag.datakey ),
		// read any passed options 
		opts = obj.data || {};
		// count another realted event
		data.related += 1;
		// extend data options bound with this event
		// don't iterate "opts" in case it is a node 
		$.each( drag.defaults, function( key, def ){
			if ( opts[ key ] !== undefined )
				data[ key ] = opts[ key ];
		});
	},
	
	// forget unbound related events
	remove: function(){
		$.data( this, drag.datakey ).related -= 1;
	},
	
	// configure interaction, capture settings
	setup: function(){
		// check for related events
		if ( $.data( this, drag.datakey ) ) 
			return;
		// initialize the drag data with copied defaults
		var data = $.extend({ related:0 }, drag.defaults );
		// store the interaction data
		$.data( this, drag.datakey, data );
		// bind the mousedown event, which starts drag interactions
		$event.add( this, "touchstart mousedown", drag.init, data );
		// prevent image dragging in IE...
		if ( this.attachEvent ) 
			this.attachEvent("ondragstart", drag.dontstart ); 
	},
	
	// destroy configured interaction
	teardown: function(){
		var data = $.data( this, drag.datakey ) || {};
		// check for related events
		if ( data.related ) 
			return;
		// remove the stored data
		$.removeData( this, drag.datakey );
		// remove the mousedown event
		$event.remove( this, "touchstart mousedown", drag.init );
		// enable text selection
		drag.textselect( true ); 
		// un-prevent image dragging in IE...
		if ( this.detachEvent ) 
			this.detachEvent("ondragstart", drag.dontstart ); 
	},
		
	// initialize the interaction
	init: function( event ){ 
		// sorry, only one touch at a time
		if ( drag.touched ) 
			return;
		// the drag/drop interaction data
		var dd = event.data, results;
		// check the which directive
		if ( event.which != 0 && dd.which > 0 && event.which != dd.which ) 
			return; 
		// check for suppressed selector
		if ( $( event.target ).is( dd.not ) ) 
			return;
		// check for handle selector
		if ( dd.handle && !$( event.target ).closest( dd.handle, event.currentTarget ).length ) 
			return;

		drag.touched = event.type == 'touchstart' ? this : null;
		dd.propagates = 1;
		dd.mousedown = this;
		dd.interactions = [ drag.interaction( this, dd ) ];
		dd.target = event.target;
		dd.pageX = event.pageX;
		dd.pageY = event.pageY;
		dd.dragging = null;
		// handle draginit event... 
		results = drag.hijack( event, "draginit", dd );
		// early cancel
		if ( !dd.propagates )
			return;
		// flatten the result set
		results = drag.flatten( results );
		// insert new interaction elements
		if ( results && results.length ){
			dd.interactions = [];
			$.each( results, function(){
				dd.interactions.push( drag.interaction( this, dd ) );
			});
		}
		// remember how many interactions are propagating
		dd.propagates = dd.interactions.length;
		// locate and init the drop targets
		if ( dd.drop !== false && $special.drop ) 
			$special.drop.handler( event, dd );
		// disable text selection
		drag.textselect( false ); 
		// bind additional events...
		if ( drag.touched )
			$event.add( drag.touched, "touchmove touchend", drag.handler, dd );
		else 
			$event.add( document, "mousemove mouseup", drag.handler, dd );
		// helps prevent text selection or scrolling
		if ( !drag.touched || dd.live )
			return false;
	},	
	
	// returns an interaction object
	interaction: function( elem, dd ){
		var offset = $( elem )[ dd.relative ? "position" : "offset" ]() || { top:0, left:0 };
		return {
			drag: elem, 
			callback: new drag.callback(), 
			droppable: [],
			offset: offset
		};
	},
	
	// handle drag-releatd DOM events
	handler: function( event ){ 
		// read the data before hijacking anything
		var dd = event.data;	
		// handle various events
		switch ( event.type ){
			// mousemove, check distance, start dragging
			case !dd.dragging && 'touchmove': 
				event.preventDefault();
			case !dd.dragging && 'mousemove':
				//  drag tolerance, x² + y² = distance²
				if ( Math.pow(  event.pageX-dd.pageX, 2 ) + Math.pow(  event.pageY-dd.pageY, 2 ) < Math.pow( dd.distance, 2 ) ) 
					break; // distance tolerance not reached
				event.target = dd.target; // force target from "mousedown" event (fix distance issue)
				drag.hijack( event, "dragstart", dd ); // trigger "dragstart"
				if ( dd.propagates ) // "dragstart" not rejected
					dd.dragging = true; // activate interaction
			// mousemove, dragging
			case 'touchmove':
				event.preventDefault();
			case 'mousemove':
				if ( dd.dragging ){
					// trigger "drag"		
					drag.hijack( event, "drag", dd );
					if ( dd.propagates ){
						// manage drop events
						if ( dd.drop !== false && $special.drop )
							$special.drop.handler( event, dd ); // "dropstart", "dropend"							
						break; // "drag" not rejected, stop		
					}
					event.type = "mouseup"; // helps "drop" handler behave
				}
			// mouseup, stop dragging
			case 'touchend': 
			case 'mouseup': 
			default:
				if ( drag.touched )
					$event.remove( drag.touched, "touchmove touchend", drag.handler ); // remove touch events
				else 
					$event.remove( document, "mousemove mouseup", drag.handler ); // remove page events	
				if ( dd.dragging ){
					if ( dd.drop !== false && $special.drop )
						$special.drop.handler( event, dd ); // "drop"
					drag.hijack( event, "dragend", dd ); // trigger "dragend"	
				}
				drag.textselect( true ); // enable text selection
				// if suppressing click events...
				if ( dd.click === false && dd.dragging )
					$.data( dd.mousedown, "suppress.click", new Date().getTime() + 5 );
				dd.dragging = drag.touched = false; // deactivate element	
				break;
		}
	},
		
	// re-use event object for custom events
	hijack: function( event, type, dd, x, elem ){
		// not configured
		if ( !dd ) 
			return;
		// remember the original event and type
		var orig = { event:event.originalEvent, type:event.type },
		// is the event drag related or drog related?
		mode = type.indexOf("drop") ? "drag" : "drop",
		// iteration vars
		result, i = x || 0, ia, $elems, callback,
		len = !isNaN( x ) ? x : dd.interactions.length;
		// modify the event type
		event.type = type;
		// remove the original event
		event.originalEvent = null;
		// initialize the results
		dd.results = [];
		// handle each interacted element
		do if ( ia = dd.interactions[ i ] ){
			// validate the interaction
			if ( type !== "dragend" && ia.cancelled )
				continue;
			// set the dragdrop properties on the event object
			callback = drag.properties( event, dd, ia );
			// prepare for more results
			ia.results = [];
			// handle each element
			$( elem || ia[ mode ] || dd.droppable ).each(function( p, subject ){
				// identify drag or drop targets individually
				callback.target = subject;
				// force propagtion of the custom event
				event.isPropagationStopped = function(){ return false; };
				// handle the event	
				result = subject ? $event.dispatch.call( subject, event, callback ) : null;
				// stop the drag interaction for this element
				if ( result === false ){
					if ( mode == "drag" ){
						ia.cancelled = true;
						dd.propagates -= 1;
					}
					if ( type == "drop" ){
						ia[ mode ][p] = null;
					}
				}
				// assign any dropinit elements
				else if ( type == "dropinit" )
					ia.droppable.push( drag.element( result ) || subject );
				// accept a returned proxy element 
				if ( type == "dragstart" )
					ia.proxy = $( drag.element( result ) || ia.drag )[0];
				// remember this result	
				ia.results.push( result );
				// forget the event result, for recycling
				delete event.result;
				// break on cancelled handler
				if ( type !== "dropinit" )
					return result;
			});	
			// flatten the results	
			dd.results[ i ] = drag.flatten( ia.results );	
			// accept a set of valid drop targets
			if ( type == "dropinit" )
				ia.droppable = drag.flatten( ia.droppable );
			// locate drop targets
			if ( type == "dragstart" && !ia.cancelled )
				callback.update(); 
		}
		while ( ++i < len )
		// restore the original event & type
		event.type = orig.type;
		event.originalEvent = orig.event;
		// return all handler results
		return drag.flatten( dd.results );
	},
		
	// extend the callback object with drag/drop properties...
	properties: function( event, dd, ia ){		
		var obj = ia.callback;
		// elements
		obj.drag = ia.drag;
		obj.proxy = ia.proxy || ia.drag;
		// starting mouse position
		obj.startX = dd.pageX;
		obj.startY = dd.pageY;
		// current distance dragged
		obj.deltaX = event.pageX - dd.pageX;
		obj.deltaY = event.pageY - dd.pageY;
		// original element position
		obj.originalX = ia.offset.left;
		obj.originalY = ia.offset.top;
		// adjusted element position
		obj.offsetX = obj.originalX + obj.deltaX; 
		obj.offsetY = obj.originalY + obj.deltaY;
		// assign the drop targets information
		obj.drop = drag.flatten( ( ia.drop || [] ).slice() );
		obj.available = drag.flatten( ( ia.droppable || [] ).slice() );
		return obj;	
	},
	
	// determine is the argument is an element or jquery instance
	element: function( arg ){
		if ( arg && ( arg.jquery || arg.nodeType == 1 ) )
			return arg;
	},
	
	// flatten nested jquery objects and arrays into a single dimension array
	flatten: function( arr ){
		return $.map( arr, function( member ){
			return member && member.jquery ? $.makeArray( member ) : 
				member && member.length ? drag.flatten( member ) : member;
		});
	},
	
	// toggles text selection attributes ON (true) or OFF (false)
	textselect: function( bool ){ 
		$( document )[ bool ? "unbind" : "bind" ]("selectstart", drag.dontstart )
			.css("MozUserSelect", bool ? "" : "none" );
		// .attr("unselectable", bool ? "off" : "on" )
		document.unselectable = bool ? "off" : "on"; 
	},
	
	// suppress "selectstart" and "ondragstart" events
	dontstart: function(){ 
		return false; 
	},
	
	// a callback instance contructor
	callback: function(){}
	
};

// callback methods
drag.callback.prototype = {
	update: function(){
		if ( $special.drop && this.available.length )
			$.each( this.available, function( i ){
				$special.drop.locate( this, i );
			});
	}
};

// patch $.event.$dispatch to allow suppressing clicks
var $dispatch = $event.dispatch;
$event.dispatch = function( event ){
	if ( $.data( this, "suppress."+ event.type ) - new Date().getTime() > 0 ){
		$.removeData( this, "suppress."+ event.type );
		return;
	}
	return $dispatch.apply( this, arguments );
};

// event fix hooks for touch events...
var touchHooks = 
$event.fixHooks.touchstart = 
$event.fixHooks.touchmove = 
$event.fixHooks.touchend =
$event.fixHooks.touchcancel = {
	props: "clientX clientY pageX pageY screenX screenY".split( " " ),
	filter: function( event, orig ) {
		if ( orig ){
			var touched = ( orig.touches && orig.touches[0] )
				|| ( orig.changedTouches && orig.changedTouches[0] )
				|| null; 
			// iOS webkit: touchstart, touchmove, touchend
			if ( touched ) 
				$.each( touchHooks.props, function( i, prop ){
					event[ prop ] = touched[ prop ];
				});
		}
		return event;
	}
};

// share the same special event configuration with related events...
$special.draginit = $special.dragstart = $special.dragend = drag;

})( jQuery );

/**
 * jQuery Roundabout - v2.4.2
 * http://fredhq.com/projects/roundabout
 *
 * Moves list-items of enabled ordered and unordered lists long
 * a chosen path. Includes the default "lazySusan" path, that
 * moves items long a spinning turntable.
 *
 * Terms of Use // jQuery Roundabout
 *
 * Open source under the BSD license
 *
 * Copyright (c) 2011-2012, Fred LeBlanc
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   - Neither the name of the author nor the names of its contributors
 *     may be used to endorse or promote products derived from this
 *     software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
(function(a){"use strict";var b,c,d;a.extend({roundaboutShapes:{def:"lazySusan",lazySusan:function(a,b,c){return{x:Math.sin(a+b),y:Math.sin(a+3*Math.PI/2+b)/8*c,z:(Math.cos(a+b)+1)/2,scale:Math.sin(a+Math.PI/2+b)/2+.5}}}});b={bearing:0,tilt:0,minZ:100,maxZ:280,minOpacity:.4,maxOpacity:1,minScale:.4,maxScale:1,duration:600,btnNext:null,btnNextCallback:function(){},btnPrev:null,btnPrevCallback:function(){},btnToggleAutoplay:null,btnStartAutoplay:null,btnStopAutoplay:null,easing:"swing",clickToFocus:true,clickToFocusCallback:function(){},focusBearing:0,shape:"lazySusan",debug:false,childSelector:"li",startingChild:null,reflect:false,floatComparisonThreshold:.001,autoplay:false,autoplayDuration:1e3,autoplayPauseOnHover:false,autoplayCallback:function(){},autoplayInitialDelay:0,enableDrag:false,dropDuration:600,dropEasing:"swing",dropAnimateTo:"nearest",dropCallback:function(){},dragAxis:"x",dragFactor:4,triggerFocusEvents:true,triggerBlurEvents:true,responsive:false};c={autoplayInterval:null,autoplayIsRunning:false,autoplayStartTimeout:null,animating:false,childInFocus:-1,touchMoveStartPosition:null,stopAnimation:false,lastAnimationStep:false};d={init:function(e,f,g){var h,i=(new Date).getTime();e=typeof e==="object"?e:{};f=a.isFunction(f)?f:function(){};f=a.isFunction(e)?e:f;h=a.extend({},b,e,c);return this.each(function(){var b=a(this),c=b.children(h.childSelector).length,e=360/c,i=h.startingChild&&h.startingChild>c-1?c-1:h.startingChild,j=h.startingChild===null?h.bearing:360-i*e,k=b.css("position")!=="static"?b.css("position"):"relative";b.css({padding:0,position:k}).addClass("roundabout-holder").data("roundabout",a.extend({},h,{startingChild:i,bearing:j,oppositeOfFocusBearing:d.normalize.apply(null,[h.focusBearing-180]),dragBearing:j,period:e}));if(g){b.unbind(".roundabout").children(h.childSelector).unbind(".roundabout")}else{if(h.responsive){a(window).bind("resize",function(){d.stopAutoplay.apply(b);d.relayoutChildren.apply(b)})}}if(h.clickToFocus){b.children(h.childSelector).each(function(c){a(this).bind("click.roundabout",function(){var e=d.getPlacement.apply(b,[c]);if(!d.isInFocus.apply(b,[e])){d.stopAnimation.apply(a(this));if(!b.data("roundabout").animating){d.animateBearingToFocus.apply(b,[e,b.data("roundabout").clickToFocusCallback])}return false}})})}if(h.btnNext){a(h.btnNext).bind("click.roundabout",function(){if(!b.data("roundabout").animating){d.animateToNextChild.apply(b,[b.data("roundabout").btnNextCallback])}return false})}if(h.btnPrev){a(h.btnPrev).bind("click.roundabout",function(){d.animateToPreviousChild.apply(b,[b.data("roundabout").btnPrevCallback]);return false})}if(h.btnToggleAutoplay){a(h.btnToggleAutoplay).bind("click.roundabout",function(){d.toggleAutoplay.apply(b);return false})}if(h.btnStartAutoplay){a(h.btnStartAutoplay).bind("click.roundabout",function(){d.startAutoplay.apply(b);return false})}if(h.btnStopAutoplay){a(h.btnStopAutoplay).bind("click.roundabout",function(){d.stopAutoplay.apply(b);return false})}if(h.autoplayPauseOnHover){b.bind("mouseenter.roundabout.autoplay",function(){d.stopAutoplay.apply(b,[true])}).bind("mouseleave.roundabout.autoplay",function(){d.startAutoplay.apply(b)})}if(h.enableDrag){if(!a.isFunction(b.drag)){if(h.debug){alert("You do not have the drag plugin loaded.")}}else if(!a.isFunction(b.drop)){if(h.debug){alert("You do not have the drop plugin loaded.")}}else{b.drag(function(a,c){var e=b.data("roundabout"),f=e.dragAxis.toLowerCase()==="x"?"deltaX":"deltaY";d.stopAnimation.apply(b);d.setBearing.apply(b,[e.dragBearing+c[f]/e.dragFactor])}).drop(function(a){var c=b.data("roundabout"),e=d.getAnimateToMethod(c.dropAnimateTo);d.allowAnimation.apply(b);d[e].apply(b,[c.dropDuration,c.dropEasing,c.dropCallback]);c.dragBearing=c.period*d.getNearestChild.apply(b)})}b.each(function(){var b=a(this).get(0),c=a(this).data("roundabout"),e=c.dragAxis.toLowerCase()==="x"?"pageX":"pageY",f=d.getAnimateToMethod(c.dropAnimateTo);if(b.addEventListener){b.addEventListener("touchstart",function(a){c.touchMoveStartPosition=a.touches[0][e]},false);b.addEventListener("touchmove",function(b){var f=(b.touches[0][e]-c.touchMoveStartPosition)/c.dragFactor;b.preventDefault();d.stopAnimation.apply(a(this));d.setBearing.apply(a(this),[c.dragBearing+f])},false);b.addEventListener("touchend",function(b){b.preventDefault();d.allowAnimation.apply(a(this));f=d.getAnimateToMethod(c.dropAnimateTo);d[f].apply(a(this),[c.dropDuration,c.dropEasing,c.dropCallback]);c.dragBearing=c.period*d.getNearestChild.apply(a(this))},false)}})}d.initChildren.apply(b,[f,g])})},initChildren:function(b,c){var e=a(this),f=e.data("roundabout");b=b||function(){};e.children(f.childSelector).each(function(b){var f,g,h,i=d.getPlacement.apply(e,[b]);if(c&&a(this).data("roundabout")){f=a(this).data("roundabout").startWidth;g=a(this).data("roundabout").startHeight;h=a(this).data("roundabout").startFontSize}a(this).addClass("roundabout-moveable-item").css("position","absolute");a(this).data("roundabout",{startWidth:f||a(this).width(),startHeight:g||a(this).height(),startFontSize:h||parseInt(a(this).css("font-size"),10),degrees:i,backDegrees:d.normalize.apply(null,[i-180]),childNumber:b,currentScale:1,parent:e})});d.updateChildren.apply(e);if(f.autoplay){f.autoplayStartTimeout=setTimeout(function(){d.startAutoplay.apply(e)},f.autoplayInitialDelay)}e.trigger("ready");b.apply(e);return e},updateChildren:function(){return this.each(function(){var b=a(this),c=b.data("roundabout"),e=-1,f={bearing:c.bearing,tilt:c.tilt,stage:{width:Math.floor(a(this).width()*.9),height:Math.floor(a(this).height()*.9)},animating:c.animating,inFocus:c.childInFocus,focusBearingRadian:d.degToRad.apply(null,[c.focusBearing]),shape:a.roundaboutShapes[c.shape]||a.roundaboutShapes[a.roundaboutShapes.def]};f.midStage={width:f.stage.width/2,height:f.stage.height/2};f.nudge={width:f.midStage.width+f.stage.width*.05,height:f.midStage.height+f.stage.height*.05};f.zValues={min:c.minZ,max:c.maxZ,diff:c.maxZ-c.minZ};f.opacity={min:c.minOpacity,max:c.maxOpacity,diff:c.maxOpacity-c.minOpacity};f.scale={min:c.minScale,max:c.maxScale,diff:c.maxScale-c.minScale};b.children(c.childSelector).each(function(g){if(d.updateChild.apply(b,[a(this),f,g,function(){a(this).trigger("ready")}])&&(!f.animating||c.lastAnimationStep)){e=g;a(this).addClass("roundabout-in-focus")}else{a(this).removeClass("roundabout-in-focus")}});if(e!==f.inFocus){if(c.triggerBlurEvents){b.children(c.childSelector).eq(f.inFocus).trigger("blur")}c.childInFocus=e;if(c.triggerFocusEvents&&e!==-1){b.children(c.childSelector).eq(e).trigger("focus")}}b.trigger("childrenUpdated")})},updateChild:function(b,c,e,f){var g,h=this,i=a(b),j=i.data("roundabout"),k=[],l=d.degToRad.apply(null,[360-j.degrees+c.bearing]);f=f||function(){};l=d.normalizeRad.apply(null,[l]);g=c.shape(l,c.focusBearingRadian,c.tilt);g.scale=g.scale>1?1:g.scale;g.adjustedScale=(c.scale.min+c.scale.diff*g.scale).toFixed(4);g.width=(g.adjustedScale*j.startWidth).toFixed(4);g.height=(g.adjustedScale*j.startHeight).toFixed(4);i.css({left:(g.x*c.midStage.width+c.nudge.width-g.width/2).toFixed(0)+"px",top:(g.y*c.midStage.height+c.nudge.height-g.height/2).toFixed(0)+"px",width:g.width+"px",height:g.height+"px",opacity:(c.opacity.min+c.opacity.diff*g.scale).toFixed(2),zIndex:Math.round(c.zValues.min+c.zValues.diff*g.z),fontSize:(g.adjustedScale*j.startFontSize).toFixed(1)+"px"});j.currentScale=g.adjustedScale;if(h.data("roundabout").debug){k.push('<div style="font-weight: normal; font-size: 10px; padding: 2px; width: '+i.css("width")+'; background-color: #ffc;">');k.push('<strong style="font-size: 12px; white-space: nowrap;">Child '+e+"</strong><br />");k.push("<strong>left:</strong> "+i.css("left")+"<br />");k.push("<strong>top:</strong> "+i.css("top")+"<br />");k.push("<strong>width:</strong> "+i.css("width")+"<br />");k.push("<strong>opacity:</strong> "+i.css("opacity")+"<br />");k.push("<strong>height:</strong> "+i.css("height")+"<br />");k.push("<strong>z-index:</strong> "+i.css("z-index")+"<br />");k.push("<strong>font-size:</strong> "+i.css("font-size")+"<br />");k.push("<strong>scale:</strong> "+i.data("roundabout").currentScale);k.push("</div>");i.html(k.join(""))}i.trigger("reposition");f.apply(h);return d.isInFocus.apply(h,[j.degrees])},setBearing:function(b,c){c=c||function(){};b=d.normalize.apply(null,[b]);this.each(function(){var c,e,f,g=a(this),h=g.data("roundabout"),i=h.bearing;h.bearing=b;g.trigger("bearingSet");d.updateChildren.apply(g);c=Math.abs(i-b);if(!h.animating||c>180){return}c=Math.abs(i-b);g.children(h.childSelector).each(function(c){var e;if(d.isChildBackDegreesBetween.apply(a(this),[b,i])){e=i>b?"Clockwise":"Counterclockwise";a(this).trigger("move"+e+"ThroughBack")}})});c.apply(this);return this},adjustBearing:function(b,c){c=c||function(){};if(b===0){return this}this.each(function(){d.setBearing.apply(a(this),[a(this).data("roundabout").bearing+b])});c.apply(this);return this},setTilt:function(b,c){c=c||function(){};this.each(function(){a(this).data("roundabout").tilt=b;d.updateChildren.apply(a(this))});c.apply(this);return this},adjustTilt:function(b,c){c=c||function(){};this.each(function(){d.setTilt.apply(a(this),[a(this).data("roundabout").tilt+b])});c.apply(this);return this},animateToBearing:function(b,c,e,f,g){var h=(new Date).getTime();g=g||function(){};if(a.isFunction(f)){g=f;f=null}else if(a.isFunction(e)){g=e;e=null}else if(a.isFunction(c)){g=c;c=null}this.each(function(){var i,j,k,l=a(this),m=l.data("roundabout"),n=!c?m.duration:c,o=e?e:m.easing||"swing";if(!f){f={timerStart:h,start:m.bearing,totalTime:n}}i=h-f.timerStart;if(m.stopAnimation){d.allowAnimation.apply(l);m.animating=false;return}if(i<n){if(!m.animating){l.trigger("animationStart")}m.animating=true;if(typeof a.easing.def==="string"){j=a.easing[o]||a.easing[a.easing.def];k=j(null,i,f.start,b-f.start,f.totalTime)}else{k=a.easing[o](i/f.totalTime,i,f.start,b-f.start,f.totalTime)}if(d.compareVersions.apply(null,[a().jquery,"1.7.2"])>=0&&!a.easing["easeOutBack"]){k=f.start+(b-f.start)*k}k=d.normalize.apply(null,[k]);m.dragBearing=k;d.setBearing.apply(l,[k,function(){setTimeout(function(){d.animateToBearing.apply(l,[b,n,o,f,g])},0)}])}else{m.lastAnimationStep=true;b=d.normalize.apply(null,[b]);d.setBearing.apply(l,[b,function(){l.trigger("animationEnd")}]);m.animating=false;m.lastAnimationStep=false;m.dragBearing=b;g.apply(l)}});return this},animateToNearbyChild:function(b,c){var e=b[0],f=b[1],g=b[2]||function(){};if(a.isFunction(f)){g=f;f=null}else if(a.isFunction(e)){g=e;e=null}return this.each(function(){var b,h,i=a(this),j=i.data("roundabout"),k=!j.reflect?j.bearing%360:j.bearing,l=i.children(j.childSelector).length;if(!j.animating){if(j.reflect&&c==="previous"||!j.reflect&&c==="next"){k=Math.abs(k)<j.floatComparisonThreshold?360:k;for(b=0;b<l;b+=1){h={lower:j.period*b,upper:j.period*(b+1)};h.upper=b===l-1?360:h.upper;if(k<=Math.ceil(h.upper)&&k>=Math.floor(h.lower)){if(l===2&&k===360){d.animateToDelta.apply(i,[-180,e,f,g])}else{d.animateBearingToFocus.apply(i,[h.lower,e,f,g])}break}}}else{k=Math.abs(k)<j.floatComparisonThreshold||360-Math.abs(k)<j.floatComparisonThreshold?0:k;for(b=l-1;b>=0;b-=1){h={lower:j.period*b,upper:j.period*(b+1)};h.upper=b===l-1?360:h.upper;if(k>=Math.floor(h.lower)&&k<Math.ceil(h.upper)){if(l===2&&k===360){d.animateToDelta.apply(i,[180,e,f,g])}else{d.animateBearingToFocus.apply(i,[h.upper,e,f,g])}break}}}}})},animateToNearestChild:function(b,c,e){e=e||function(){};if(a.isFunction(c)){e=c;c=null}else if(a.isFunction(b)){e=b;b=null}return this.each(function(){var f=d.getNearestChild.apply(a(this));d.animateToChild.apply(a(this),[f,b,c,e])})},animateToChild:function(b,c,e,f){f=f||function(){};if(a.isFunction(e)){f=e;e=null}else if(a.isFunction(c)){f=c;c=null}return this.each(function(){var g,h=a(this),i=h.data("roundabout");if(i.childInFocus!==b&&!i.animating){g=h.children(i.childSelector).eq(b);d.animateBearingToFocus.apply(h,[g.data("roundabout").degrees,c,e,f])}})},animateToNextChild:function(a,b,c){return d.animateToNearbyChild.apply(this,[arguments,"next"])},animateToPreviousChild:function(a,b,c){return d.animateToNearbyChild.apply(this,[arguments,"previous"])},animateToDelta:function(b,c,e,f){f=f||function(){};if(a.isFunction(e)){f=e;e=null}else if(a.isFunction(c)){f=c;c=null}return this.each(function(){var g=a(this).data("roundabout").bearing+b;d.animateToBearing.apply(a(this),[g,c,e,f])})},animateBearingToFocus:function(b,c,e,f){f=f||function(){};if(a.isFunction(e)){f=e;e=null}else if(a.isFunction(c)){f=c;c=null}return this.each(function(){var g=a(this).data("roundabout").bearing-b;g=Math.abs(360-g)<Math.abs(g)?360-g:-g;g=g>180?-(360-g):g;if(g!==0){d.animateToDelta.apply(a(this),[g,c,e,f])}})},stopAnimation:function(){return this.each(function(){a(this).data("roundabout").stopAnimation=true})},allowAnimation:function(){return this.each(function(){a(this).data("roundabout").stopAnimation=false})},startAutoplay:function(b){return this.each(function(){var c=a(this),e=c.data("roundabout");b=b||e.autoplayCallback||function(){};clearInterval(e.autoplayInterval);e.autoplayInterval=setInterval(function(){d.animateToNextChild.apply(c,[b])},e.autoplayDuration);e.autoplayIsRunning=true;c.trigger("autoplayStart")})},stopAutoplay:function(b){return this.each(function(){clearInterval(a(this).data("roundabout").autoplayInterval);a(this).data("roundabout").autoplayInterval=null;a(this).data("roundabout").autoplayIsRunning=false;if(!b){a(this).unbind(".autoplay")}a(this).trigger("autoplayStop")})},toggleAutoplay:function(b){return this.each(function(){var c=a(this),e=c.data("roundabout");b=b||e.autoplayCallback||function(){};if(!d.isAutoplaying.apply(a(this))){d.startAutoplay.apply(a(this),[b])}else{d.stopAutoplay.apply(a(this),[b])}})},isAutoplaying:function(){return this.data("roundabout").autoplayIsRunning},changeAutoplayDuration:function(b){return this.each(function(){var c=a(this),e=c.data("roundabout");e.autoplayDuration=b;if(d.isAutoplaying.apply(c)){d.stopAutoplay.apply(c);setTimeout(function(){d.startAutoplay.apply(c)},10)}})},normalize:function(a){var b=a%360;return b<0?360+b:b},normalizeRad:function(a){while(a<0){a+=Math.PI*2}while(a>Math.PI*2){a-=Math.PI*2}return a},isChildBackDegreesBetween:function(b,c){var d=a(this).data("roundabout").backDegrees;if(b>c){return d>=c&&d<b}else{return d<c&&d>=b}},getAnimateToMethod:function(a){a=a.toLowerCase();if(a==="next"){return"animateToNextChild"}else if(a==="previous"){return"animateToPreviousChild"}return"animateToNearestChild"},relayoutChildren:function(){return this.each(function(){var b=a(this),c=a.extend({},b.data("roundabout"));c.startingChild=b.data("roundabout").childInFocus;d.init.apply(b,[c,null,true])})},getNearestChild:function(){var b=a(this),c=b.data("roundabout"),d=b.children(c.childSelector).length;if(!c.reflect){return(d-Math.round(c.bearing/c.period)%d)%d}else{return Math.round(c.bearing/c.period)%d}},degToRad:function(a){return d.normalize.apply(null,[a])*Math.PI/180},getPlacement:function(a){var b=this.data("roundabout");return!b.reflect?360-b.period*a:b.period*a},isInFocus:function(a){var b,c=this,e=c.data("roundabout"),f=d.normalize.apply(null,[e.bearing]);a=d.normalize.apply(null,[a]);b=Math.abs(f-a);return b<=e.floatComparisonThreshold||b>=360-e.floatComparisonThreshold},getChildInFocus:function(){var b=a(this).data("roundabout");return b.childInFocus>-1?b.childInFocus:false},compareVersions:function(a,b){var c,d=a.split(/\./i),e=b.split(/\./i),f=d.length>e.length?d.length:e.length;for(c=0;c<=f;c++){if(d[c]&&!e[c]&&parseInt(d[c],10)!==0){return 1}else if(e[c]&&!d[c]&&parseInt(e[c],10)!==0){return-1}else if(d[c]===e[c]){continue}if(d[c]&&e[c]){if(parseInt(d[c],10)>parseInt(e[c],10)){return 1}else{return-1}}}return 0}};a.fn.roundabout=function(b){if(d[b]){return d[b].apply(this,Array.prototype.slice.call(arguments,1))}else if(typeof b==="object"||a.isFunction(b)||!b){return d.init.apply(this,arguments)}else{a.error("Method "+b+" does not exist for jQuery.roundabout.")}}})(jQuery)